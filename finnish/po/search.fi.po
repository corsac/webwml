msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 18:05+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/searchtmpl/search.def:6
msgid "Search for"
msgstr "Hakusanat"

#: ../../english/searchtmpl/search.def:10
msgid "Results per page"
msgstr "tuloksia per sivu"

#: ../../english/searchtmpl/search.def:14
msgid "Output format"
msgstr "Tulostusmuoto"

#: ../../english/searchtmpl/search.def:18
msgid "Long"
msgstr "pitkä"

#: ../../english/searchtmpl/search.def:22
msgid "Short"
msgstr "lyhyt"

#: ../../english/searchtmpl/search.def:26
msgid "URL"
msgstr "URL"

#: ../../english/searchtmpl/search.def:30
msgid "Default query type"
msgstr "Oletushakutyyppi"

#: ../../english/searchtmpl/search.def:34
msgid "All Words"
msgstr "kaikki sanat"

#: ../../english/searchtmpl/search.def:38
msgid "Any Words"
msgstr "mikä tahansa sanoista"

#: ../../english/searchtmpl/search.def:42
msgid "Search through"
msgstr "Hakualue"

#: ../../english/searchtmpl/search.def:46
msgid "Entire site"
msgstr "koko sivusto"

#: ../../english/searchtmpl/search.def:50
msgid "Language"
msgstr "kieli"

#: ../../english/searchtmpl/search.def:54
msgid "Any"
msgstr "mikä tahansa"

#: ../../english/searchtmpl/search.def:58
msgid "Search results"
msgstr "Hakutulokset"

#: ../../english/searchtmpl/search.def:65
msgid ""
"Displaying documents \\$(first)-\\$(last) of total <B>\\$(total)</B> found."
msgstr ""
"Näytetään dokumentit \\$(first)-\\$(last) löytyneestä <B>\\$(total)</B>."

#: ../../english/searchtmpl/search.def:69
msgid ""
"Sorry, but search returned no results. <P>Some pages may be available in "
"English only, you may want to try searching again and set the Language to "
"Any."
msgstr ""
"Valitettavasti tällä haulla ei löytänyt mitään. <P>Osa sivuista on "
"saatavilla ainoastaan englanniksi, joten voit halutessasi asettaa kieleksi "
"\"mikä tahansa\" ja yrittää etsiä uudelleen."

#: ../../english/searchtmpl/search.def:73
msgid "An error occured!"
msgstr "Tapahtui virhe!"

#: ../../english/searchtmpl/search.def:77
msgid "You should give at least one word to search for."
msgstr "Anna ainakin yksi hakusana."

#: ../../english/searchtmpl/search.def:81
msgid "Powered by"
msgstr "Käytössä"

#: ../../english/searchtmpl/search.def:85
msgid "Next"
msgstr "seuraava"

#: ../../english/searchtmpl/search.def:89
msgid "Prev"
msgstr "edellinen"
